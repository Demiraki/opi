#include <iostream>
#include<string.h>
#include <string.h>

using namespace std;

struct Export {
	char product[30];
	char country[30];
	long int quantity;
}e[25];

int main(void) {
	setlocale(LC_ALL, "ru");

	char prod[20];
	short unsigned count_rec = 0;
	short unsigned i = 0;
	while (1) {
		cout << " \n Название продукта \t";
		cin >> prod;

		if (strcmp(prod, "#") == 0) break;
		strcpy_s(e[i].product, prod);

		cout << "\nСтрана\t";
		cin >> e[i].country;
		cout << "\nКоличество\t";
		cin >>e[i].quantity;

		count_rec++; i++;
	}
	cout << "\nДля продолжения нажмите что-нибудь\n\n";
	system("pause");

	cout << "Выборка:\n\n";
	int average, sum = 0, a=0;
	char filter[20];
	
	cout << "\nВведите выборку\n";
	cin >> filter;
	for (int i = 0; i < count_rec; i++) {
		if (strcmp(e[i].product, filter) == 0)
		{
			cout << "\n" << e[i].country;
			cout << "\t" << e[i].quantity;
			sum += e[i].quantity;
			a++;
		}
	}
	average = sum / a;
	cout << "\nСредний объем экспорта\t" << average;
	return 0;
}

